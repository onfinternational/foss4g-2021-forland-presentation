---
theme: gaia
class:
  - lead
style: |
  :root {
    --color-background-code: #ccc;
    --color-background-paginate: rgba(128, 128, 128, 0.05);
    --color-foreground: #000;
    --color-highlight: #fff;
    --color-highlight-hover: #030;
    --color-highlight-heading: #093;
    --color-_header: #bbb;
    --color-header-shadow: transparent;
    font-size: 30px;
  }


paginate: true
backgroundImage: url('')
marp: true
---
<!--
_color: white;
-->
# **How Open Source solution help to build a scalable and adaptable environmental decision platform tools**
![bg 100%](Pictures/onfi_bckg.jpg)

Buenos Aires 2021-09-30

---
<!--
_footer: ![w:70](https://www.onfinternational.org/wp-content/uploads/2020/04/ONFi_logotype_A_pantone.png)
-->
# Begin by the end :)
![bg 100%](Pictures/forland_paragominas.png)

---
<!--
_footer: ![w:70](https://www.onfinternational.org/wp-content/uploads/2020/04/ONFi_logotype_A_pantone.png)
_color: Black
-->
# Outline
- Context
- Requirements
- Success story
- What’s next ?


---
<!--
_footer: ![w:70](https://www.onfinternational.org/wp-content/uploads/2020/04/ONFi_logotype_A_pantone.png)
_color: Black
-->
# Outline
- ***Context***
- Requirements
- Success story
- What’s next ?


---
<!--
_header:
_footer: ![w:70](https://www.onfinternational.org/wp-content/uploads/2020/04/ONFi_logotype_A_pantone.png)
_color: Black
-->
## Context - ONFI activity types 1/2
![bg left:33% ](Pictures/phot_guaviare_1.jpg)
- Subsidiary of the French National Forest Office (ONF)
- Involved in forest managment, monitoring
- Institutional body, international cooperation projects on the one hand
- Competitive player as an expertise consultancy firm on the other.

---
<!--
_header:
_footer: ![w:70](https://www.onfinternational.org/wp-content/uploads/2020/04/ONFi_logotype_A_pantone.png)
_color: Black
-->
![bg left:33%](Pictures/phot_guaviare_1.jpg)
## Context - ONFI activity types 2/2
Project involving different...
- Actors (NGO, municipality, country, farmers,...)
- Experts (Forester, Farmer, forest manager, botanist, dont' forget the geomatician )
- Size: from 1 expert to 40 people.



---
<!--
_header:
_footer: ![w:70](https://www.onfinternational.org/wp-content/uploads/2020/04/ONFi_logotype_A_pantone.png)
_color: Black
-->
## Context -  ONFI main skills
- Project management
- Forest managment
- Rural development
- Biodiversity
- Stakeholders engagment
- Climate change
- Remote sensing
- GIS, Database...

---
<!--
_footer: ![w:70](https://www.onfinternational.org/wp-content/uploads/2020/04/ONFi_logotype_A_pantone.png)
_color: Black
-->
# Outline
- Context
- ***Requirements***
- Success story
- What’s next ?

---
<!--
_header:
_footer: ![w:70](https://www.onfinternational.org/wp-content/uploads/2020/04/ONFi_logotype_A_pantone.png)
_color: Black
-->
# Requirementss
1. How to help projects ?
2. What set of tools ?

---
<!--
_header: Requirementss
_footer: ![w:70](https://www.onfinternational.org/wp-content/uploads/2020/04/ONFi_logotype_A_pantone.png)
_color: Black
-->
![bg left:15% h:632](Pictures/ForlandIdeas.png)
## How to help projects ?
### Current situation
- Democratization of Internet access
- Abundance of data, especially geographic
- Increasingly sophisticated data exploitation and analysis methods
- ***However***, limited accessibility to new information technologies ***for most people***
- A lot of projects with big reports without any update that end up in a drawer !!


---
<!--
_header: Requirementss
_footer: ![w:70](https://www.onfinternational.org/wp-content/uploads/2020/04/ONFi_logotype_A_pantone.png)
_color: Black
-->
![bg left:15% h:632](Pictures/ForlandIdeas.png)
## How to help projects ?
### Current situation
- **Need** in territorial development projects to share information with all stakeholders to make effective and efficient decisions.
- **Objectives of a platform called Forland**
Develop a digital tool that:
  - ***facilitates*** the technology transfer
  - ***promotes*** the development of skills
  - ***democratizes*** the use of geospatial data and analysis tools in decision-making for territorial projects. 


---
<!--
_header: Requirementss
_footer: ![w:70](https://www.onfinternational.org/wp-content/uploads/2020/04/ONFi_logotype_A_pantone.png)
_color: Black
-->
![bg right:30% w:400](Pictures/forland_2.png)
## What set of tools - User point of view ?
- "All-in-one" web tool 
- User friendly interface
- Give access to the latest information technologies and spatial data analysis
- Promotes participatory processes
- Facilitates decision-making
- Promotes transparency and communication in territorial projects.

---
<!--
_header: Requirementss
_footer: ![w:70](https://www.onfinternational.org/wp-content/uploads/2020/04/ONFi_logotype_A_pantone.png)
_color: Black
-->
![bg right:20% w:250](Pictures/forland_1.png)
## What set of tools - Dev point of view ?
- Open Source, updated, community
- For Geek not dev !
- Easy to deploy in existing server
- Know language programming (Python, R, Javascript)
- With 3 main components 
**Mapping** / **Dashboard** / **Processing**
- Easy to adapt
- **Not a blackbox at 1 000 000 $ !**

---
<!--
_header: Requirementss
_footer: ![w:70](https://www.onfinternational.org/wp-content/uploads/2020/04/ONFi_logotype_A_pantone.png)
_color: Black
-->
## What set of tools - The Choice

- We chose Lizmap [![LizMap w:50](https://www.3liz.com/images/logo-lizmap.png)](https://www.3liz.com/opensource.html#lizmap) from 3Liz [![3Liz w:50](https://www.3liz.com/images/flavicon.png)](https://www.3liz.com/opensource.html#lizmap)
- Based on  [![QGIS h:40](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c2/QGIS_logo%2C_2017.svg/langfr-1920px-QGIS_logo%2C_2017.svg.png)](https://www.qgis.org)  et [![Postgresql h:50](https://si.cen-occitanie.org/wp-content/uploads/2016/01/postgresql-logo.png)](https://www.postgresql.org) / [![Postgis h:50](https://postgis.net/images/postgis-logo.png)](https://postgis.net/) and [![Plotly h:46](https://plotly-marketing-website.cdn.prismic.io/plotly-marketing-website/948b6663-9429-4bd6-a4cc-cb33231d4532_logo-plotly.svg)](https://plotly.com/)
- Includind OGC standard:  WPS connected to QGIS 
- **Check all the Requirementss :)**

---
<!--
_footer: ![w:70](https://www.onfinternational.org/wp-content/uploads/2020/04/ONFi_logotype_A_pantone.png)
_color: Black
-->
# Outline
- Context
- Requirements
- ***Success story***
- What’s next ?

---
<!--
_header:
_footer: ![w:70](https://www.onfinternational.org/wp-content/uploads/2020/04/ONFi_logotype_A_pantone.png)
_color: Black
-->
# Success story
1. The story
2. The worflow
3. The platform !
4. Feedback of users

---
<!--
_header: Success story
_footer: ![w:70](https://www.onfinternational.org/wp-content/uploads/2020/04/ONFi_logotype_A_pantone.png)
_color: Black
-->
![bg right:45% w:550 drop-shadow:0,5px,10px,rgba(0,0,0,.4)](Pictures/guav2.jpg)
## The story
- Caminemos, Territorios Sostenibles project in the region of Guaviare in Columbia (South East)
- Peace agreement in the area
- Illegal Crops
- Demobilisation and reintegration of former rebels
- **Needs for development support**

---
<!--
_header: Success story
_footer: ![w:70](https://www.onfinternational.org/wp-content/uploads/2020/04/ONFi_logotype_A_pantone.png)
_color: Black
-->
## The worflow - with user
**Step 1/2**
![height:500px drop-shadow:0,5px,10px,rgba(0,0,0,.4)](Pictures/Forland_Workflow_Diagram_part1.png)

---
<!--
_header: Success story
_footer: ![w:70](https://www.onfinternational.org/wp-content/uploads/2020/04/ONFi_logotype_A_pantone.png)
_color: Black
-->
## The worflow - with user
**Step 2/2**
![height:500px drop-shadow:0,5px,10px,rgba(0,0,0,.4)](Pictures/Forland_Workflow_Diagram_part2.png)


---
<!--
_header: Success story
_footer: ![w:70](https://www.onfinternational.org/wp-content/uploads/2020/04/ONFi_logotype_A_pantone.png)
_color: Black
-->
## The worflow - Internal
![height:400px drop-shadow:0,5px,10px,rgba(0,0,0,.4)](Pictures/Forland_Internal_Workflow_Diagram.png)

---
<!--
_header: Success story
_footer: ![w:70](https://www.onfinternational.org/wp-content/uploads/2020/04/ONFi_logotype_A_pantone.png)
_color: Black
-->
## Go to the Platform
[![height:600px drop-shadow:0,5px,10px,rgba(0,0,0,.4)](Pictures/Forland_login_page.png)](https://guaviare.forland.io) 


---
<!--
_header: Success story
_footer: ![w:70](https://www.onfinternational.org/wp-content/uploads/2020/04/ONFi_logotype_A_pantone.png)
_color: Black
-->
## The Platform - Login page
![height:600px drop-shadow:0,5px,10px,rgba(0,0,0,.4)](Pictures/Forland_login_page.png)

---
<!--
_header: Success story
_footer: ![w:70](https://www.onfinternational.org/wp-content/uploads/2020/04/ONFi_logotype_A_pantone.png)
_color: Black
-->
## The Platform - Project list page
![height:600px drop-shadow:0,5px,10px,rgba(0,0,0,.4)](Pictures/Forland_project_page.png)

---
<!--
_header: Success story
_footer: ![w:70](https://www.onfinternational.org/wp-content/uploads/2020/04/ONFi_logotype_A_pantone.png)
_color: Black
-->
## The Platform - Main project page
![height:600px drop-shadow:0,5px,10px,rgba(0,0,0,.4)](Pictures/Forland_Main_Interface_description.png)

---
<!--
_header: Success story
_footer: ![w:70](https://www.onfinternational.org/wp-content/uploads/2020/04/ONFi_logotype_A_pantone.png)
_color: Black
-->
## The Platform - Map page
![height:600px drop-shadow:0,5px,10px,rgba(0,0,0,.4)](Pictures/info_table_edit.png)

---
<!--
_header: Success story
_footer: ![w:70](https://www.onfinternational.org/wp-content/uploads/2020/04/ONFi_logotype_A_pantone.png)
_color: Black
-->
## The Platform - Edit page
![height:600px drop-shadow:0,5px,10px,rgba(0,0,0,.4)](Pictures/edit_point.jpg)

---
<!--
_header: Success story
_footer: ![w:70](https://www.onfinternational.org/wp-content/uploads/2020/04/ONFi_logotype_A_pantone.png)
_color: Black
-->
## The Platform - Vector filter page
![height:600px drop-shadow:0,5px,10px,rgba(0,0,0,.4)](Pictures/filter_form.png)

---
<!--
_header: Success story
_footer: ![w:70](https://www.onfinternational.org/wp-content/uploads/2020/04/ONFi_logotype_A_pantone.png)
_color: Black
-->
## The Platform - Dashboard page 1/3
![height:600px drop-shadow:0,5px,10px,rgba(0,0,0,.4)](Pictures/dashboard.png)

---
<!--
_header: Success story
_footer: ![w:70](https://www.onfinternational.org/wp-content/uploads/2020/04/ONFi_logotype_A_pantone.png)
_color: Black
-->
## The Platform - Dashboard page 2/3
![height:600px drop-shadow:0,5px,10px,rgba(0,0,0,.4)](Pictures/forland_graph.jpg)

---
<!--
_header: Success story
_footer: ![w:70](https://www.onfinternational.org/wp-content/uploads/2020/04/ONFi_logotype_A_pantone.png)
_color: Black
-->
## The Platform - Dashboard page 3/3
![height:600px drop-shadow:0,5px,10px,rgba(0,0,0,.4)](Pictures/dashboard_media.png)

---
<!--
_header: Success story
_footer: ![w:70](https://www.onfinternational.org/wp-content/uploads/2020/04/ONFi_logotype_A_pantone.png)
_color: Black
-->
## The Platform - Processing page 1/2
![height:600px drop-shadow:0,5px,10px,rgba(0,0,0,.4)](Pictures/processing.png)

---
<!--
_header: Success story
_footer: ![w:70](https://www.onfinternational.org/wp-content/uploads/2020/04/ONFi_logotype_A_pantone.png)
_color: Black
-->
## The Platform - Processing page 2/2
![height:600px drop-shadow:0,5px,10px,rgba(0,0,0,.4)](Pictures/processing_result.png)

---
<!--
_header: Success story
_footer: ![w:70](https://www.onfinternational.org/wp-content/uploads/2020/04/ONFi_logotype_A_pantone.png)
_color: Black
-->
## The Platform - Help page
![height:600px drop-shadow:0,5px,10px,rgba(0,0,0,.4)](Pictures/forland_help.jpg)

---
<!--
_header: Success story
_footer: ![w:70](https://www.onfinternational.org/wp-content/uploads/2020/04/ONFi_logotype_A_pantone.png)
_color: Black
-->
## Users feedbacks
![bg right:50% h:300 drop-shadow:0,5px,10px,rgba(0,0,0,.4)](Pictures/guav2.jpg)
![bg h:300 drop-shadow:0,5px,10px,rgba(0,0,0,.4)](Pictures/guav3.jpg)
![bg h:300 drop-shadow:0,5px,10px,rgba(0,0,0,.4)](Pictures/guav1.jpg)
- Help for data sharing and analysis
- Help to connect different stakeholders
- Help to manage project activities
- **Final World** : "The project is still finished and TODAY we used the platform"



---
<!--
_footer: ![w:70](https://www.onfinternational.org/wp-content/uploads/2020/04/ONFi_logotype_A_pantone.png)
_color: Black
-->
# Outline
- Context
- Requirements
- Success story
- ***What’s next ?***

---
<!--
_header:
_footer: ![w:70](https://www.onfinternational.org/wp-content/uploads/2020/04/ONFi_logotype_A_pantone.png)
_color: Black
-->
# What's next ?
![bg right:40% h:300 drop-shadow:0,5px,10px,rgba(0,0,0,.4)](Pictures/forland_feedbacks.jpg)
![bg h:300 drop-shadow:0,5px,10px,rgba(0,0,0,.4)](Pictures/games_pgm.jpg)

- **Continuous improvement**
- Integrate internal Javascript overlay **like user feedback** to Lizmap Feature
- Improvement for **better tool integration for stakeholders engagement**
 Example: integrate information of board games
- Add relevant Open Source tools


---
<!--
_footer: ![w:70](https://www.onfinternational.org/wp-content/uploads/2020/04/ONFi_logotype_A_pantone.png)
-->
# Thank you for attention :D !
# Thanks to Open Source especially the 3Liz Team for the help and its motivation !
![bg 100%](Pictures/Guaviare_wallpaper.jpg)

---
<!--
_header: I
_footer:z
-->
![bg 100%](Pictures/onfi_end.png)