# FOSS4G 2021 Forland presentation

Presentation done in [FOSS4G 2021](https://callforpapers.2021.foss4g.org/foss4g2021/speaker/T3QHKX/)

**How Open Source solution help to build an scalable and adaptable environmental decision platform tools**

ONF International, an institutional body in matters of international cooperation projects in the forest management areas need tools to help people to empower their decision. The Open-Source solution help us to find a set of tools to develop a web-based platform to interact with different stakeholder and help them on their decision about forest management. Especially we used and fund some component of QGIS, py-qgis-server, py-qgis-wps, R through a work with 3Liz using Lizmap. A successful work, funded by the Climate KIC was done on building a platform to help stakeholder on a forest restoration process on the Paragominas municipality in Brazil.

------

ONF International is a private company working with private company as well as NGO or ministry at international level about forest management.

These activities required to be able that all people involved in a forest management process can discuss together around a common denominator, a dedicated platform.

Indeed, manage forest landscape required different kind of work such as to spatialize the space as well as display its own area such as the farm, regional park or municipality. It’s also often necessary to be able to generate statistic over specific information or being to be able to simulate and analyze scenario such as where are the best areas to do forest restoration in terms of biodiversity.

Consequently, we need a set of tools allowing us to integrate all these requirements. Important parameter is that these set of tools must be scalable, quite easy to manage and be cost-efficient. So it quite naturally that we go through Open Source tools and more specifically Lizmap and its based component such as QGIS, py-qgis-server, py-qgis-wps, R.

Through Lizmap, such tools allow us to design a platform, called Forland that contain the following component: * Webmapping including edition tools * Web processing * Web reporting

Webmapping component allow us to visualize all geographic data given by the owner or produced by ourselves. In addition, this component is useful to allow user to interact with the data using edition tool.

Web processing is especially powerful because based on exchange with the owner or more generally the different stakeholder it always to design a customized tool that user can use in the platform in order to help discussion and decision.

Web reporting is a component allowing user to export map as well as statistic or graph as a summary of its use of the platform.

At the end, web technologies used in Lizmap allow us to customize the interface using for example JavaScript to add feedback tools or add button to get access to a documentation component.

In the context of a Climate KIC project and in collaboration with Lizmap for the development tools and XXXX for the design, we fund the development of these platform in the context of the forest restoration in Paragominas municipality in Brazil. All stakeholder was convinced by the adaptability of the platform to help to take decision.

Different new project continues to enhance and develop our Forland platform where the structure is based on Open-Source tools

------

**Authors and Affiliations** –Cédric Lardeux (1), Maxence Rageade (1), René-Luc D'Hont (2)
(1) ONF International
(2) 3Liz
